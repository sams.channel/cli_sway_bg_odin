### cli\_sway\_bg
> Simple CLI application (Odin implementation) to set the background when using [swaywm](https://swaywm.org).

##### How to use

> You will need to install the [Odin compiler](http://odin-lang.org), [make](https://www.gnu.org/software/make) utility
and [UPX](https://upx.github.io) executable compressor to build from repository source.

> It is recommend to build the [Odin compiler from source](http://odin-lang.org/docs/install), retrieved with Git.
> I recommend building on [Ubuntu](https://ubuntu.com/download) (or a [Docker container](https://gitlab.com/sams.channel/compilers) or server VM of Ubuntu).

On Ubuntu - you can install the following packages, before building the compiler, with `APT`):
<pre># apt install upx-ucl clang llvm build-essential</pre>

##### Build from source and install
<pre>$ make</pre>
<pre># make install</pre>

##### Usage

<pre>$ cli_sway_bg bg=/path/to/wallpaper_or_background_image.jpg</pre>

Then press Mod+Shift+c to refresh swaywm and, if valid, your specified wallpaper will now be set.
