/**
    * CLI application to set background/wallpaper on a system using swaywm.
    * Author: Sam St-Pettersen, 2023
    * Released under the MIT License
*/

package main

import "core:fmt"
import "core:os"
import "core:strings"

setBackground :: proc(program: string, bg: string, cfg: string) -> int {
    // Check that background file exists,
    // if not throw an error.
    if !os.exists(bg) {
        return displayError(program,
        fmt.aprintf("Background file does not exist: \"%s\"", bg))
    }

    // Read in file amd set background on applicable line.
    // Push each line to out array.
    out := make([dynamic]u8, 0, 0)
    data, ok := os.read_entire_file(cfg, context.allocator)
    if !ok {
        return displayError(program,
        fmt.aprintf("Could not read configuration file: \"%s\"", cfg))
    }
    defer delete(data, context.allocator)

    it := string(data)
    for line in strings.split_lines_iterator(&it) {
        l := line
        if strings.has_prefix(l, "output * bg") {
            l = fmt.aprintf("output * bg \"%s\" fill", bg)
        }
        append(&out, fmt.aprintf("%s\n", l))
    }

    // Write out replacement file.
    os.write_entire_file(cfg, out[:], false)

    fmt.printf("Set background to:\n\"%s\".\n", bg)
    fmt.println("Press Mod+Shift+c to refresh swaywm settings.")

    return 0
}

displayError :: proc(program: string, err: string) -> int {
    fmt.printf("Error: %s.\n\n", err)
    return displayUsage(program, -1)
}

displayUsage :: proc(program: string, exitCode: int) -> int {
    fmt.println("CLI utility to set background/wallpaper on a system using swaywm.")
    fmt.println("Written by Sam St-Pettersen <s.stpettersen@proton.me>\n")
    fmt.printf("Usage: %s OPTION\n\n", program)
    fmt.println("Options:")
    fmt.println("bg=PATH_TO_BG_IMAGE   Specify the background/wallpaper to set.\n")
    return exitCode
}

main :: proc() {
    exitCode: int = 0
    program : string : "cli_sway_bg"

    // Detect a Windows system.
    if os.get_env("OS") == "Windows_NT" {
        fmt.println("This program is only intended for Unix-like OSes.")
        os.exit(exitCode)
    }

    // Check that swaywm is installed via detecting its configuration.
    user: string = os.get_env("USER")
    cfg: string = fmt.aprintf("/home/%s/.config/sway/config", user)

    if !os.exists(cfg) {
        fmt.println("Error: swaywm not detected.")
        fmt.println("Please install it and create your configuration")
        fmt.println("under ~/.config/sway/\n")
        exitCode = displayUsage(program, -1)
        os.exit(exitCode)
    }

    if len(os.args) > 1 {
        for a in os.args {
            if strings.has_prefix(a, "bg=") {
                bg, _ := strings.replace(a, "bg=", "", 1)
                exitCode = setBackground(program, bg, cfg)
                os.exit(exitCode)
            }
            else if !strings.has_prefix(a, program) {
                exitCode = displayError(program,
                fmt.aprintf("\"%s\" is not a valid option", a))
                os.exit(exitCode)
            }
        }
    }
    else {
        exitCode = displayUsage(program, 0)
    }

    os.exit(exitCode)
}
